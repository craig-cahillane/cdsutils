# -*- makefile -*-

PACKAGE = cdsutils

VERSION := $(shell git describe | sed -e 's/_/~/' -e 's/-/+/' -e 's/-/~/' -e 's|.*/||')
VFILE := lib/cdsutils/_version.py
CVERS := $(shell test -e $(VFILE) && python -c "execfile('$(VFILE)'); print __version__")

unexport IFO

##########

.PHONY: all
all: build

##########

.PHONY: test-cdsutils
test-cdsutils: export PYTHONPATH := lib:$(PYTHONPATH)
test-cdsutils:
	python -m cdsutils.tests $(OPTS)

.PHONY: test
test: test-cdsutils

##########

.PHONY: version _release-commit release-major release-minor release-rev release
version:
	echo "__version__ = '$(VERSION)'" >$(VFILE)
_release-commit: version
	git add $(VFILE)
	git commit -m "Release $(VERSION)"
	git tag -m "Release" $(VERSION)
release-major: VERSION := $(shell python -c \
	"import version as v; print v._release('$(VERSION)', 'major')")
release-major: _release-commit
release-minor: VERSION := $(shell python -c \
	"import version as v; print v._release('$(VERSION)', 'minor')")
release-minor: _release-commit
release-rev: VERSION := $(shell python -c \
	"import version as v; print v._release('$(VERSION)', 'rev')")
release-rev: _release-commit
release: release-rev

##########

.PHONY: dist
dist: version
	python ./setup.py sdist

.PHONY: build
build: version
	python ./setup.py build

.PHONY: install
ifndef PREFIX
install: version
	python ./setup.py install
else
install: version
	python ./setup.py install --prefix=$(PREFIX)
endif

##################################################

ligo-install: APPSROOT := $(or $(APPSROOT), /ligo/apps/linux-x86_64)
ligo-install: DISTDIR := $(APPSROOT)/$(PACKAGE)-$(VERSION)
ligo-install: ENVSCRIPT := $(DISTDIR)/etc/cdsutils-user-env.sh
ligo-install: version
	python ./setup.py install --prefix=$(DISTDIR)
	install -d $(DISTDIR)/etc
	printf 'export PATH=%s/bin:$$PATH\n' $(DISTDIR) >$(ENVSCRIPT)
	printf 'export PYTHONPATH=%s/lib/python2.6/site-packages:$$PYTHONPATH\n' $(DISTDIR) >>$(ENVSCRIPT)
	printf 'export PYTHONPATH=%s/lib/python2.7/site-packages:$$PYTHONPATH\n' $(DISTDIR) >>$(ENVSCRIPT)

##########

# Debian/Ubuntu test package
.PHONY: deb-snapshot
# FIXME: do this without creating a dist tarball
deb-snapshot: DIST := $(PACKAGE)-$(VERSION)
deb-snapshot: dist
	mkdir -p build
	cd build; tar zxf ../dist/$(DIST).tar.gz
	cp -a debian build/$(DIST)
	cd build/$(DIST); dch -b -v $(VERSION) -D UNRELEASED 'test build, not for upload'
	cd build/$(DIST); echo '3.0 (native)' > debian/source/format
	cd build/$(DIST); debuild -us -uc

.PHONY: deb
deb: VERSION := $(shell git describe master)
deb: DIST := $(PACKAGE)-$(VERSION)
deb: dist
	rm -rf build/$(VERSION)
	rm -rf build/$(DIST)
	mkdir -p build/$(VERSION)
	git archive master | tar -x -C build/$(VERSION)/
	cd build/$(VERSION) && python setup.py sdist
	mv build/$(VERSION)/dist/$(DIST).tar.gz build/$(PACKAGE)_$(VERSION).orig.tar.gz
	cd build && tar xf $(PACKAGE)_$(VERSION).orig.tar.gz
	mkdir build/$(DIST)/debian
	git archive debian:debian | tar -x -C build/$(DIST)/debian/
	cd build/$(DIST) && debuild -uc -us

##########

.PHONY: clean
clean:
	rm -rf dist
	rm -rf build
