cdsutils
========

Various python libraries and command line utilities for interacting
with the Advanced LIGO control system.

This fork was created for the addition of pzlockin, adding a general real-time demodulation scheme for easy signal phasing.
