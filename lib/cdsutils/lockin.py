=======
#!/usr/bin/env python

####
# lockin.py
#   lockin python script. this code uses tf of cdsutils. 
#   by Masayuki Nakano
#   Oct 28, 2013
#######################################
#
# Code updated by Craig Cahillane, Hang Yu, and Rana Adhikari for addition to cdsutils.
# December 10, 2018
#
#######################################
# This code is not completed (Oct 31,2013)
# 1.turn_sine_on, stop_sine function used tds fucntion(ezsine).  it should  be replased by awg function.
# 2.When error happen, it shoult stop the stimulus
# 3.Plot function for check the result
#####

import sys
import time
import math
import cmath
import subprocess
import epics as ep
import nds2
import numpy as np
import tf
import awg
import argparse
import signal

class SIGINT_handler():
    '''Class for handling Ctrl+C'''
    def __init__(self):
        self.SIGINT = False
        self.awg_sine = None

    def signal_handler(self, signal, frame):
        print('You pressed Ctrl+C!')
        print('Handling exit...\n')
        self.SIGINT = True
        if not self.awg_sine == None:
            turn_sine_off(self.awg_sine)
        print('Exiting')
        sys.exit(0)

def turn_sine_on(channel, ff, amp, timeout, printflg = False):
    '''Turns on the sine wave excitation'''
    sine = awg.Sine(channel, freq=ff, ampl=amp)
    sine.start(ramptime=0.5)
    timestamp = time.time()
    script_on = False
    
    while ep.caget(channel+'MON') == 0:
        if not script_on:
            print 'Turning on sine wave'
            script_on = True
            time.sleep(0.5)
    if printflg:
        print 'START: freq = ' + str(ff) + ', amp = ' + str(amp) + ', chan = ' + channel
    return sine

def turn_sine_off(awg_sine):
    '''Turns off sine wave excitation, should run at end of script or when user pressed Ctrl+C'''
    awg_sine.stop(ramptime=0.5)
    print 'Excitation turned off\n'
    return

def myfit(x,y,weight):
    xarray = np.array(x)
    yarray = np.array(y)
    warray = np.array(weight)

    xsum = sum(xarray*warray)
    ysum = sum(yarray*warray)
    wsum = sum(warray)

    xsqr = sum(xarray*xarray*warray)
    ysqr = sum(yarray*yarray*warray)
    msqr = sum(xarray*yarray*warray)

    xavg = xsum/wsum
    yavg = ysum/wsum

    xx = xsqr/wsum - xavg*xavg
    yy = ysqr/wsum - yavg*yavg
    xy = msqr/wsum - xavg*yavg

    slp = xx/yy
    xint = xavg - yavg/slp
    yint = yavg - xavg*slp

    e = xarray *slp + yint - yarray
    esum = sum(e * e / warray)
    yvar = esum/wsum

    return slp,xint,yint,xavg,yavg,yvar

def fit_result(rslt):
    rslt = np.array(rslt)
    x = rslt[:,8]
    re = rslt[:,3]
    im = rslt[:,4]
    w = rslt[:,7]

    [reSlp,xreInt,reInt,xAvg,reAvg,reVar] = myfit(x,re,w)
    [imSlp,ximInt,imInt,xAvg,imAvg,imVar] = myfit(x,im,w)

    #phase for zero slope orthogonal
    phi = np.arctan(imSlp/reSlp)

    #offsets
    actOff = reAvg * np.cos(phi) + imAvg * np.sin(phi)
    ortOff = imAvg * np.cos(phi) - reAvg * np.sin(phi)

    #actuation slope and x intercept
    actSlp = np.sqrt(reSlp * reSlp +imSlp*imSlp)
    actInt = xAvg - actOff / actSlp

    #error bars
    yErr = np.sqrt(reVar+imVar)
    xErr = yErr / actSlp
    return actSlp, actInt, phi * 180 / np.pi, ortOff, xAvg, yErr, xErr

######Main function
def lockin(freq,
           amp,
           stim_chan,
           meas_chan,
           act_chan,
           act_step,
           start_step = None,
           N_step = 10,
           read_chan = False,
           N_avg = 100,
           N_cycl = 10,
           goodCoMin = 0.8):
    '''Starts the excitation, measures the response, demodulates at a single frequency, and records the FFT and coherence for every phase step''' 
    # Initialize Ctrl+C handler 
    handler = SIGINT_handler()
    signal.signal(signal.SIGINT, handler.signal_handler)
          
    #If readback channel is not defined set it to stimulate channel(default)
    if not read_chan:
        read_chan = stim_chan

    #prepare the array for result
    demodrslt = []
    
    #read initial actuation records
    print "\n=== Initial Values"
    initVal = ep.caget(act_chan)
    print act_chan + " = " + str(initVal)

    if not start_step:
        start_step = initVal - N_step * act_step / 2.

    #set stimtime
    stimtime = int(N_avg * N_cycl / freq * (N_step + 1) + 20)
    print "\n==== Approximate Duration " + str(stimtime) + " seconds"
    
    ep.caput(act_chan,start_step)
    mWeNorm = None
    
    ############ Start Stimulus #########
    print "\n==== Start Stimulus"
    
    sine = turn_sine_on(stim_chan, freq, amp, stimtime, True)
   
    ############ Start Measurements ######### 
    ii = 0
    while ii < N_step + 1:
        if handler.SIGINT:
            turn_sine_off(sine)
            print('Exiting...\n')
            sys.exit(0)
        print "\n\n=== Start Measurement at " + str(ep.caget(act_chan))

        #Demodulate the output signal and take average.
        demodtmp = tf.tf(read_chan, meas_chan, freq, N_cycl, N_avg)
       
        wetmp = np.sqrt( demodtmp[0]*demodtmp[0] / (demodtmp[5]*demodtmp[5]+demodtmp[6]*demodtmp[6]) )
        if not mWeNorm:
            mWeNorm = wetmp
        wetmp = wetmp/mWeNorm
        demodrslt.append(np.append(demodtmp,[wetmp,ep.caget(act_chan)]))

        print "\n=== Measurement {}/{} OK".format(ii+1, N_step+1)
        print "coherence = " + str(demodtmp[0]) + ", weight = " + str(wetmp)
        print "mag = " +str(demodtmp[1]) + " angle = " +str(demodtmp[2])
        print "real = " +str(demodtmp[3]) + " imag = " +str(demodtmp[4])
        print "real err= " +str(demodtmp[5]) + " imag err = " +str(demodtmp[6])

        #Step next value
        ep.caput(act_chan,start_step + (ii + 1) * act_step)
        ii += 1

    ################Stop stimulus###################################################
    # Stop the stimulus.
    turn_sine_off(sine)
    ###############################################################################

    # Step to start position
    print '\n=== Measurement Done. Returning to original position.'   
    ep.caput(act_chan, initVal)

    #compute results
    [slp, crs, phase, off, xAvg, yErr, xErr] = fit_result(demodrslt)
    maxMag = max(np.array(demodrslt)[:,1])
    print '\n=== Measurement Results'
    print ' slope = ' + str(slp) + ', x-intercept = ' +str(crs) + ' x-error = ' + str(xErr)
    print ' phase = ' + str(phase) + 'degree, orthogonal coupling = ' + str(off) + ', max coupling = ' + str(maxMag)
    print ' data: (x, re, im, wight)'
    for data in demodrslt:
        print data[8], data[3], data[4], data[7]
    if xErr > abs(abs(crs) - initVal):
        if abs(abs(crs) - initVal) < 0.1:
            print 'Result is very small'
            print 'No change'
        else:
            print 'WARNING: x-error is too large to step with confidence'
            ans = raw_input(" Step to x-intercept?\ny/n")
            while not (ans == 'y' or ans == 'n'):
                ans = raw_input("\nPlease ansewer by 'y' or 'n'\n")
            if ans == 'n':
                print '\No change'
                ep.caput(act_chan,initVal)
            else:
                print '\Change to ' + str(crs)
                ep.caput(act_chan,crs)
    elif abs(abs(crs) - abs(xAvg)) > 3 * abs(act_step):
        print 'WARNING: Large step computed.'
        ans = raw_input(" Step to x-intercept?\ny/n")
        while not (ans == 'y' or ans == 'n'):
            ans = raw_input("\nPlease ansewer by 'y' or 'n'\n")
        if ans == 'n':
            print '\No change'
            ep.caput(act_chan,initVal)
        else:
            print '\Change to ' + str(crs)
            ep.caput(act_chan,crs)
    else:
        print crs
        ep.caput(act_chan,crs)

summary = lockin.__doc__.splitlines()[0]

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=lockin.__doc__.strip(),
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-c', dest = 'N_cycl',
                        help = 'define number of cycles on each step. Default is 10', 
                        type = int, default = 10)
    parser.add_argument('-a', dest = 'N_avg',
                        help = 'define number of average. Default is 100', 
                        type = int, default = 100)
    parser.add_argument('freq', help = 'stimulus frequency',
                        type = float)
    parser.add_argument('amp', help = 'stimulus amplitude',
                        type = float)
    parser.add_argument('stim_chan', help='stimulus channel',
                        type = str) 
    parser.add_argument('meas_chan', help='measurement channel',
                        type = str)
    parser.add_argument('act_chan', help='actuation channel',
                        type = str)
    parser.add_argument('act_step', help='actuation step',
                        type = float)
    parser.add_argument('-ns',dest = 'N_step',
                        help='actuation step number. default is 10',
                        type = int, default = 10)
    parser.add_argument('-s', dest = 'step_start',
                        help = 'start position of actuation channel. Default is (initial value) - N_step * act_step',
                        type = float,default = None)
    parser.add_argument('-r', dest = 'read_chan',
                        help = 'read back channel. Default is stimulus channel',
                        type = str, default = None)
    parser.add_argument('-coh', dest = 'coherence_threshold',
                        help = 'Threshold of coherence achieved to accept a data point. Default = 0.8',
                        type = float, default = 0.8)   

    args = parser.parse_args()


    lockin(args.freq,
           args.amp,
           args.stim_chan,
           args.meas_chan,
           args.act_chan,
           args.act_step,
           args.step_start,
           args.N_step,
           args.read_chan,
           args.N_avg,
           args.N_cycl,
           args.coherence_threshold)
