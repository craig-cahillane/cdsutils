import sys
import threading

import gobject
gobject.threads_init()
import pygst
pygst.require('0.10')
from math import log, exp, pi
from time import sleep

import nds

class MainLoopThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.loop = gobject.MainLoop(None, False)
        self.daemon = True

    def run(self):
        self.loop.run()
        
def audio(channel, filt=None, fixed_gain=None, pan=0.0, agc_setpt=0.2, agc_ugf=1):
    """play NDS channel as audio stream

Produces a real-time audio stream of an NDS testpoint channel.

If no fixed_gain argument is provided, a logarithmic auto gain control
(AGC) will be used.  Otherwise, fixed_gain should be a float that will
used to scale the signal amplitude.

    """

    # FIXME: we have to import gst and foton here, inside the
    # function, because they somehow capture argv and does their own
    # argument parsing, which interferes with any CLI argparse.
    # Should figure out another way around this issue.
    import gst
    from foton import Filter, FilterDesign

    player = gst.parse_launch('appsrc name=src ! queue ! audioconvert ! audioresample ! audiopanorama name=pano ! autoaudiosink')
    src = player.get_by_name('src')
    pano = player.get_by_name('pano')
    pano.set_property('panorama', pan)
    loop_thread = MainLoopThread()
    loop_thread.start()
    
    first_run = True
    conn = nds.connection()
    print >>sys.stderr, "playing %s (C-c to exit)..." % (channel)
    iterate_args = [[channel,]]
    if conn.get_protocol() == 1:
        iterate_args = [-1, [channel,]]
    for bufs in conn.iterate(*iterate_args):
        samples = bufs[0].data
        sample_rate = bufs[0].channel.sample_rate
        if filt:
            if first_run:
                filt = Filter(FilterDesign(filt, rate=sample_rate))
            samples = filt.apply(samples)
        if fixed_gain:
            # fixed gain
            samples *= 2**15*fixed_gain
        else:
            # logarithmic AGC
            lim = 1e-18
            if first_run:
                agc = log(2**15*agc_setpt) - log(max(max(abs(samples)), lim))
            for n in range(samples.size):
                samples[n] *= exp(agc)
                err = log(2**15*agc_setpt) - log(max(abs(samples[n]), lim))
                agc += 2*pi*agc_ugf*err/sample_rate
                agc = min(max(agc, log(lim)), -log(lim))
        buf = gst.Buffer(samples.astype('int16'))
        caps = gst.caps_from_string('audio/x-raw-int,rate=%i,endianness=1234,channels=1,width=16,depth=16,signed=true' % sample_rate)
        buf.set_caps(caps)
        src.emit('push-buffer', buf)
        if first_run:
            # fill up the queue in case of network glitches
            sleep(samples.size/float(sample_rate))
            player.set_state(gst.STATE_PLAYING)
            first_run = False

##################################################

# a summary used by main CLI
summary = audio.__doc__.splitlines()[0]

if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description=audio.__doc__.strip(),
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('channel', type=str,
                        help="channel to play")
    parser.add_argument('-f', '--filt', type=str, default='',
                        help="foton design string for filter to apply to audio stream")
    parser.add_argument('-g', '--fixed-gain', type=float,
                        help="use fixed gain")
    parser.add_argument('-p', '--pan', type=float, default=0.0,
                        help="position in stereo panorama (-1.0 left -> 1.0 right)")
    parser.add_argument('--agc-setpt', type=float, default=0.2,
                        help="setpoint for auto gain control servo (as fraction of full scale)")
    parser.add_argument('--agc-ugf', type=float, default=1,
                        help="UGF for auto gain control servo")
    args = parser.parse_args()

    audio(channel=args.channel,
          filt=args.filt,
          fixed_gain=args.fixed_gain,
          pan=args.pan,
          agc_setpt=args.agc_setpt,
          agc_ugf=args.agc_ugf
          )
