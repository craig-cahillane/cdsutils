# std packgages
import numpy as np
import scipy.signal as sig
from time import sleep
from matplotlib.mlab import detrend_mean
import signal

# ligo packages outside cdsutils
import awg
from awg import AWGError
from ezca import Ezca

# other modules in cdsutils
import getdata
import sfft

class LOCKIN(object):
    def __init__(self, tuneChan, excChan, outChan, inChan=None,\
            freq=30, amp=1, nStep=6, stepSize=0.1, \
            tMeas=8, tFFT=2, fs=-1, excTramp=1, tuningTramp=1, cohThres=0.8, \
            maxTrial=3, rejGlitch=False, smartScan=False):

        self.ez=Ezca()
        self.ifo=self.ez.prefix

        # channels
        if self.ifo in tuneChan:
            tuneChan=tuneChan[3:]
        self.tuneChan=tuneChan

        if inChan == None:
            inChan = excChan
        if self.ifo not in excChan:
            excChan = self.ifo + excChan 
        if self.ifo not in outChan:
            outChan = self.ifo + outChan
        if self.ifo not in inChan:
            inChan = self.ifo + inChan
        self.excChan=excChan
        self.outChan=outChan
        self.inChan=inChan

        # dither and fft options
        self.freq=freq
        self.amp=amp
        self.nStep=nStep
        self.stepSize=stepSize
        self.tMeas=tMeas
        self.tFFT=2.**np.round(np.log2(tFFT))
        if fs<=0:
            self.fs=2.**np.round(np.log2(8.*freq))
        else:
            self.fs=fs

        self.excTramp=excTramp
        self.tuningTramp=tuningTramp
        self.cohThres=cohThres

        # lockin opts
        self.maxTrial=maxTrial
        self.rejGlitch=rejGlitch
        self.smartScan=smartScan

        # current values
        self.tuneChanOldValue=self.ez[tuneChan]


    ### excitation related ###

    def create_exc(self):
        exc=awg.Sine(self.excChan, ampl=self.amp, freq=self.freq)
        self.exc=exc
        return exc

    def exc_on(self):
        if not hasattr(self, 'exc'):
            self.create_exc()

        try:
            self.exc.start(ramptime=self.excTramp)
        except AWGError:
            self.create_exc()
            self.exc.start(ramptime=self.excTramp)

    def exc_off(self):
        self.exc.stop(ramptime=self.excTramp)


    ### measurement ###

    def get_one_measure(self):

        inData, outData=getdata.getdata( [self.inChan, self.outChan], self.tMeas )

        fsIn=inData.sample_rate
        fsOut=outData.sample_rate

        inData=detrend_mean(inData.data)
        outData=detrend_mean(outData.data)

        inData=sig.resample_poly(inData, up=self.fs, down=fsIn, window='blackmanharris')
        outData=sig.resample_poly(outData, up=self.fs, down=fsOut, window='blackmanharris')

        fs=self.fs

        # glitch check; hard coded threshold to be 10 times the rms
        rejThres=10
        out_rms=np.sqrt(outData.dot(outData)/outData.size)
        out_abs_max=np.max(np.abs(outData))
        if out_abs_max > (rejThres*out_rms):
            print 'glitched during measurement!'
            print out_rms, out_abs_max
            glitch = True
        else:
            glitch = False

        nfft=2.**np.round(np.log2(fs*self.tFFT))
        res=1./self.tFFT

        tf, freq=sfft.sftfe(inData, outData, self.freq, \
                NFFT=nfft, res=res, Fs=fs, detrend=detrend_mean, overlap=0.5)
        coh, freq=sfft.sfcohere(inData, outData, self.freq, \
                NFFT=nfft, res=res, Fs=fs, detrend=detrend_mean, overlap=0.5)

        # need to double check the math here
        SNR = coh/(1.-coh)

        # check the measurement quality
        if coh<self.cohThres:
            goodFlag=False
        elif (self.rejGlitch and glitch):
            goodFlag=False
        else:
            goodFlag=True

        return freq, np.abs(tf), np.angle(tf), coh, goodFlag


    def scan_gain(self, gainCntr):
        signal.signal(signal.SIGINT, self.signal_handler)
        I_resp = np.zeros(self.nStep)
        Q_resp = np.zeros(self.nStep)
        snr = np.zeros(self.nStep)

        # initiate a list of gain to be scanned and put the gain to the first element of the list
        print 'generating a list of gains to be scanned'
        if np.mod(self.nStep, 2) == 0:
            gainList=np.linspace(gainCntr - (self.nStep/2. - 0.5)*self.stepSize, \
                                 gainCntr + (self.nStep/2. - 0.5)*self.stepSize, \
                                 self.nStep)

            for i in range(1, self.nStep/2+1, 1):
                self.ez[self.tuneChan] = gainList[self.nStep/2 - i]
                sleep(self.tuningTramp)


        else:
            gainList=np.linspace(gainCntr - (self.nStep-1)/2. * self.stepSize, \
                                 gainCntr + (self.nStep+1)/2. * self.stepSize, \
                                 self.nStep)

            for i in range(1, (self.nStep+1)/2, 1):
                self.ez[self.tuneChan] = gainList[(self.nStep-1)/2 - i]
                sleep(self.tuningTramp)

        print 'list of the gains'
        print gainList

        # start scaning
        print 'Start scanning'
        for i in range(self.nStep):
            sleep(self.tuningTramp)
            if i>0:
                self.ez[self.tuneChan]=gainList[i]
                sleep(2.*self.tuningTramp)

            cnt = 0
            goodFlag = False

            while (cnt<self.maxTrial) and (not goodFlag):
                __, mag, ang, coh, goodFlag=self.get_one_measure()
                print coh, goodFlag, (cnt<self.maxTrial) and (not goodFlag)
                cnt += 1

            I_resp[i], Q_resp[i] = mag*np.cos(ang), mag*np.sin(ang)
            snr[i] = np.sqrt(coh/(1.-coh))

        self.ez[self.tuneChan] = gainCntr
        sleep(self.tuningTramp)

        return gainList, I_resp, Q_resp, snr


    def run_lockin_safe(self):
        signal.signal(signal.SIGINT, self.signal_handler)

        print 'creating an awg'
        self.exc_on()

        optFlag=False
        gainCntr=self.tuneChanOldValue

        while not optFlag:
            gainList, I_resp, Q_resp, snr = self.scan_gain(gainCntr)
            optGain = self.find_opt_gain(gainList, I_resp, Q_resp, snr)
            
            if ( optGain >= (np.min(gainList)-self.stepSize) ) \
              and ( optGain <= (np.max(gainList)+self.stepSize) ):
                print 'optimal gain found!'
                self.ez[self.tuneChan] = np.round(optGain, 3)
                sleep(self.tuningTramp)
                optFlag = True

            elif optGain < (np.min(gainList)-self.stepSize):
                self.ez[self.tuneChan] = np.min(gainList)
                sleep(self.tuningTramp)
                gainCntr = np.min(gainList)

            else:
                self.ez[self.tuneChan] = np.max(gainList)
                sleep(self.tuningTramp) 
                gainCntr = np.max(gainList)

        print 'turning off the excitation'
        self.exc_off()

        print 'updating the gain'
        self.tuneChanOldValue=np.round(optGain, 3)


    def lockin_measure_only(self, gainCntr=None):
        signal.signal(signal.SIGINT, self.signal_handler)

        print 'creating an awg'
        self.exc_on()
        
        if gainCntr==None:
            gainCntr=self.tuneChanOldValue

        gainList, I_resp, Q_resp, snr = self.scan_gain(gainCntr)
        optGain = self.find_opt_gain(gainList, I_resp, Q_resp, snr)
        print 'optimal gain setting'
        print optGain

        print 'turning off the excitation'
        self.exc_off()
        return optGain

    ### auxiliary ###

    def find_opt_gain(self, gainList, I_resp, Q_resp, snr):
        I_coef=np.polyfit(gainList, I_resp, deg=1, w=snr)
        Q_coef=np.polyfit(gainList, Q_resp, deg=1, w=snr)

        mtrx=np.zeros([2,2])
        mtrx[0,0], mtrx[0,1]=I_coef[0], I_coef[1]
        mtrx[1,0], mtrx[1,1]=Q_coef[0], Q_coef[1]

        mtrx=self.rotate_mtrx(mtrx)
        optGain=-mtrx[0, 1]/mtrx[0, 0]

        return optGain


    def rotate_mtrx(self, M):
        theta=np.arctan(M[1, 0]/M[0, 0])
        rotM=np.zeros([2,2])
        rotM[0, 0] = np.cos(theta)
        rotM[0, 1] = np.sin(theta)
        rotM[1, 0] = -np.sin(theta)
        rotM[1, 1] = np.cos(theta)
        newM = np.dot(rotM, M)
        return newM

    def signal_handler(self, signal, frame):
        print 'process killed!'
        print 'turning off excitation!'
        self.exc_off()
        print 'recovering the old value!'
        self.ez[self.tuneChan]=self.tuneChanOldValue



### command line ###
if __name__ == '__main__':
    import argparse    
    par = argparse.ArgumentParser(description='lockin parser')
    # must be specified by user
    par.add_argument('tuneChan', 
            help="channel whose gain will be tuned", 
            type=str)
    par.add_argument('excChan', 
            help="channel to send in the excitation", 
            type=str)
    par.add_argument('outChan', 
            help="channel to read the output signal", 
            type=str)
    par.add_argument('freq', 
            help="freq of the measurement", 
            type=np.float)
    par.add_argument('amp', 
            help="amplitude of the excitation", 
            type=np.float)

    # w/ default values; only for advanced usage
    par.add_argument('--inChan', 
            help="channel to be used as the input signal to compute the tf to the output, [default:excChan]", 
            type=str, default='None')
    par.add_argument('--nStep', 
            help="number of points used for scanning the tuneChan, [default:6]", 
            type=np.int, default=6)
    par.add_argument('--stepSize', 
            help="step size for the scanning, [default:0.1]", 
            type=np.float, default=0.1)
    par.add_argument('--tMeas', 
            help="duration of data in sec to be collected at each scanning point, [default:8]", 
            type=np.float, default=8)
    par.add_argument('--tFFT', 
            help="length of each fft segment in sec for each fft segment, [default:2]", 
            type=np.float, default=2)
    par.add_argument('--fs', 
            help="sampling rate of the data used for estimating the fft, [default:2**round(log2(8*freq))]", 
            type=np.float, default=-1)
    par.add_argument('--excTramp', 
            help="ramping time of turning on the excitation, [default:1]", 
            type=np.float, default=1)
    par.add_argument('--tuningTramp', 
            help="wait this long between each tuning point, [default:1]", 
            type=np.float, default=1)
    par.add_argument('--cohThres', 
            help="coherence threshold to be considered as a good measurement, [default:0.8]", 
            type=np.float, default=0.8)
    par.add_argument('--maxTrial', 
            help="max trials to repeat before a good measurement, [default:3]", 
            type=np.int, default=3)
    par.add_argument('--rejGlitch', 
            help="reject measurements with gliches in it, <=0 for False, [default:-1]", 
            type=np.float, default=-1)
    par.add_argument('--smartScan', 
            help="doing fancy ways for tuning the gain, <=0 for False, [default:-1]", 
            type=np.float, default=-1)
    par.add_argument('--scanOnly',
            help="only do a single set of scanning and then calculating the optimal gain. The tune channel is then reverted to its original value and let the user to deside if want to go to the calculated optimal point, <=0 for False, [default:-1]", 
            type=np.float, default=-1)
    par.add_argument('--singleMeasurement', 
            help="turning on the excitation without tuning; only do a single measurement at the current gain level and print out the results, <=0 for False, [default:-1]", 
            type=np.float, default=-1)

    args = par.parse_args()

    ### FIXME ###
    # should change to the **kwargs format
    tuneChan, excChan, outChan, freq, amp = args.tuneChan, args.excChan, args.outChan, args.freq, args.amp

    inChan=args.inChan
    if inChan == 'None':
        inChan=None

    nStep, stepSize = args.nStep, args.stepSize
    tMeas, tFFT, fs = args.tMeas, args.tFFT, args.fs
    excTramp, tuningTramp=args.excTramp, args.tuningTramp
    cohThres=args.cohThres
    maxTrial=args.maxTrial
    rejGlitch=args.rejGlitch
    smartScan=args.smartScan

    print stepSize
    
    lockin = LOCKIN(tuneChan, excChan, outChan, inChan=inChan,\
            freq=freq, amp=amp, nStep=nStep, stepSize=stepSize, \
            tMeas=tMeas, tFFT=tFFT, fs=fs, excTramp=excTramp, tuningTramp=tuningTramp, cohThres=cohThres, \
            maxTrial=maxTrial, rejGlitch=rejGlitch, smartScan=smartScan)

    signal.signal(signal.SIGINT, lockin.signal_handler)
    
    if args.scanOnly > 0:
        print 'scan only!'
        lockin.lockin_measure_only()
    elif args.singleMeasurement > 0:
        lockin.exc_on()
        sleep(excTramp+tuningTramp)
        freq, mag, ang, coh, goodFlag=lockin.get_one_measure()
        lockin.exc_off()
