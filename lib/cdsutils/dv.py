#!/usr/bin/env python

import argparse
import signal

import pydv

##################################################

description = """plot time series of NDS channels

Requires that the 'tconvert' tcl script (part of ligotools) is in the
path.
"""
summary = description.splitlines()[0]
#usage = "dv [<options>] <channel> [<channel>...]"

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=description.strip(),
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('channel_names', metavar='channel', nargs='+', type=str, help='channel name')
    parser.add_argument('--debug', action='store_true', help='flag for very verbose output')
    parser.add_argument('--subplots', action='store_true', help='make a separate subplot for each channel')
    parser.add_argument('--sharex', action='store_true', help='share the x axis on all subplots')
    parser.add_argument('--sharey', action='store_true', help='share the y axis on all subplots')
    parser.add_argument('--time', type=float, default=None, help='end time for all channels (default: now)')
    parser.add_argument('--title', type=str, default=None, help='plot title')
    parser.add_argument('--lookback', type=float, default=10, help='time to look behind --time')
    parser.add_argument('--lookforward', type=float, default=0, help='time to look ahead of --time')
    parser.add_argument('--no-threshold', action='store_true', help='do not try to parse threshold channel names')
    parser.add_argument('--no-wd-state', action='store_true', help='do not try to parse watchdog state channel names')
    args = parser.parse_args()

    if args.subplots:
        newChannelNames = []
        for channelName in args.channel_names:
            newChannelNames.append([channelName])
        args.channel_names = newChannelNames
    else:
        args.channel_names = [args.channel_names]

    if args.time is None:
        from subprocess import check_output
        args.time = int(check_output(["tconvert", "now"]))

    # for cleaner C-c handling
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    pydv.plot(
        channel_names=args.channel_names,
        xmin=args.time-args.lookback,
        xmax=args.time+args.lookforward,
        center_time=args.time,
        title=args.title,
        sharex=args.sharex,
        sharey=args.sharey,
        no_threshold=args.no_threshold,
        no_wd_state=args.no_wd_state,
        )
