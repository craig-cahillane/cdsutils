from setuptools import setup

version = {}
with open("lib/cdsutils/_version.py") as f:
    exec(f.read(), version)

setup(
    name = 'cdsutils',
    version = version['__version__'],
    description = 'Advanced LIGO CDS python utilities',
    author = 'Jameson Graef Rollins',
    author_email = 'jameson.rollins@ligo.org',
    url = 'https://git.ligo.org/cds/cdsutils',
    license = 'GPL v3+',
    keywords = [],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
    ],

    package_dir = {'': 'lib'},
    packages = [
        'cdsutils',
    ],

    # https://chriswarrick.com/blog/2014/09/15/python-apps-the-right-way-entry_points-and-scripts/
    # should we have a 'gui_scripts' as well?
    entry_points={
        'console_scripts': [
            'cdsutils = cdsutils.__main__:main',
        ],
    },

)
